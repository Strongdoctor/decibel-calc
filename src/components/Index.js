import React, { Component } from 'react';
import IndexInputSection from './IndexInputSection';
import DecibelAfterAmplification from './DecibelAfterAmplification';
import { IndexContext } from './IndexContext';

class Index extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    if(process.env.NODE_ENV === "development") {
      console.log("Index props:", this.props)
      console.log("Index state:", this.state)
    }
    
    return (
      <div className="container">
        <header className="text-center">
          <h1 className="mb-0">SPL Calculator</h1>
          <span className="h5">( <a href="https://gitlab.com/Strongdoctor/decibel-calc">source code</a> )</span>
        </header>
        <main>
          <IndexContext.Consumer>
            {(context) => (
              <IndexInputSection context={context} />
            )}
          </IndexContext.Consumer>
          
          <IndexContext.Consumer>
            {({
              speakerSensitivity,
              powerIntoSpeaker,
              distanceFromSpeaker,
              amountOfSpeakers
            }) => (
              <DecibelAfterAmplification
                speakerSensitivity = {speakerSensitivity}
                powerIntoSpeaker  ={powerIntoSpeaker}
                distanceFromSpeaker = {distanceFromSpeaker}
                amountOfSpeakers = {amountOfSpeakers}
              />
            )}
          </IndexContext.Consumer>
        </main>
      </div>
    )
  }
}

export default Index;