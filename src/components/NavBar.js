import React, { Component } from 'react';

class NavBar extends Component {
    render() {
        return(
            <nav className="pt-5">
                <ul className="nav nav-pills nav-fill">
                    <li className="nav-item">
                        <a className="nav-link active" href="#">Active</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">Longer nav link</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">Link</a>
                    </li>
                </ul>
            </nav>
        );
    }
}

export default NavBar;