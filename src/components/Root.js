import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Index from './Index';
import { IndexContext } from './IndexContext';

class Root extends Component {
    constructor(props) {
        super(props);

        this.updateSpeakerSensitivity = this.updateSpeakerSensitivity.bind(this);
        this.updatePowerIntoSpeaker = this.updatePowerIntoSpeaker.bind(this);
        this.updateDistanceFromSpeaker = this.updateDistanceFromSpeaker.bind(this);
        this.updateAmountOfSpeakers = this.updateAmountOfSpeakers.bind(this);
        
        this.state = {
            speakerSensitivity: "",
            powerIntoSpeaker: "",
            distanceFromSpeaker: "",
            amountOfSpeakers: "",
            updateSpeakerSensitivity: this.updateSpeakerSensitivity,
            updatePowerIntoSpeaker: this.updatePowerIntoSpeaker,
            updateDistanceFromSpeaker: this.updateDistanceFromSpeaker,
            updateAmountOfSpeakers: this.updateAmountOfSpeakers
        }
    }

    updateSpeakerSensitivity(event) {
        if(process.env.NODE_ENV === "development") {
            console.log("updateSpeakerSensitivity",event.target.value);
        }

        this.setState({
          speakerSensitivity: event.currentTarget.value
        })
      }

      updatePowerIntoSpeaker(event) {
        if(process.env.NODE_ENV === "development") {
            console.log("updatePowerIntoSpeaker",event.target.value);
        }

        this.setState({
          powerIntoSpeaker: event.currentTarget.value
        })
      }
    
      updateDistanceFromSpeaker(event) {
        if(process.env.NODE_ENV === "development") {
            console.log("updateDistanceFromSpeaker",event.target.value);
        }
        this.setState({
          distanceFromSpeaker: event.currentTarget.value
        })
      }

      updateAmountOfSpeakers(event) {
        if(process.env.NODE_ENV === "development") {
            console.log("updateAmountOfSpeakers",event.target.value);
        }

        this.setState({
            amountOfSpeakers: event.currentTarget.value
        })
      }

    render() {
        return(
            <IndexContext.Provider value={this.state}>
                <Router>
                    <Route exact path="/" component={Index} />
                </Router>
            </IndexContext.Provider>
        )
    }
}

export default Root;