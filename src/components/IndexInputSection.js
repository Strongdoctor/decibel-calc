import React, { Component } from 'react';

class IndexInputSection extends Component {
    constructor(props) {
        super(props);
        if(process.env.NODE_ENV === "development") {
            console.log("IndexInputSection Props", this.props)
        }
    }

    render() {
        return (
            <div className="container pt-5">
                <div className="row">
                    <div className="col-sm-6 col-lg-4 col-xl-3 pt-4">
                        <div className="d-flex h-100 flex-column justify-content-between">
                            <div>Speaker Sensitivity (Decibels)</div>
                            <h5>Speaker loudness at 1W power, 1 unit of distance away.</h5>
                            <input
                                className="text-center form-control mt-2"
                                type="text"
                                placeholder="86"
                                onChange={this.props.context.updateSpeakerSensitivity}
                                value={this.props.context.speakerSensitivity}
                                pattern="[0-9]+"
                                min="1"
                            />
                        </div>
                    </div>
                    <div className="col-sm-6 col-lg-4 col-xl-3 pt-4">
                        <div className="d-flex h-100 flex-column justify-content-between">
                            <div>Power into speakers (Watts)</div>
                            <h5>Loudness increases by 3dB per doubling of power.</h5>
                            <input
                                className="text-center form-control mt-2"
                                type="text"
                                placeholder="1"
                                onChange={this.props.context.updatePowerIntoSpeaker}
                                value={this.props.context.powerIntoSpeaker}
                                pattern="[0-9]+"
                                min="1"
                            />
                        </div>
                    </div>
                    <div className="col-sm-6 col-lg-4 col-xl-3 pt-4">
                        <div className="d-flex h-100 flex-column justify-content-between">
                            <div>Distance from speaker</div>
                            <h5>Loudness decreases by 6dB per doubling of distance.</h5>
                            <input
                                className="text-center form-control mt-2"
                                type="text"
                                placeholder="1"
                                onChange={this.props.context.updateDistanceFromSpeaker}
                                value={this.props.context.distanceFromSpeaker}
                                pattern="[0-9]+"
                                min="1"
                            />
                        </div>
                    </div>
                    <div className="col-sm-6 col-lg-4 col-xl-3 pt-4">
                        <div className="d-flex h-100 flex-column justify-content-between">
                            <div>Amount of speakers</div>
                            <h5>Each doubling of speakers increases dB by 3.</h5>
                            <input
                                className="text-center form-control mt-2"
                                type="text"
                                placeholder="1"
                                onChange={this.props.context.updateAmountOfSpeakers}
                                value={this.props.context.amountOfSpeakers}
                                pattern="[0-9]+"
                                min="1"
                            />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default IndexInputSection;