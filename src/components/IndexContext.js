import React from 'react';

export const IndexContext = React.createContext({
    speakerSensitivity: 0,
    powerIntoSpeaker: 1,
    distanceFromSpeaker: 1,
    amountOfSpeakers: 1,
    updateSpeakerSensitivity: () => {},
    updatePowerIntoSpeaker: () => {},
    updateDistanceFromSpeaker: () => {},
    updateAmountOfSpeakers: () => {}
});