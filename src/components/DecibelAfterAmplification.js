import React, { Component } from "react";

class DecibelAfterAmplification extends Component {
  constructor(props) {
    super(props);
  }

  compensateForDistance(db, distance) {
      return db - (20 * Math.log10(parseFloat(distance)));
  }

  render() {
    console.log("Results props:", this.props);

    var adjustedDecibel;
    var speakerSensitivity, powerIntoSpeaker, distanceFromSpeaker, amountOfSpeakers;
    
    if(this.props.speakerSensitivity === "") {
      speakerSensitivity = 86;
    } else {
      speakerSensitivity = parseFloat(this.props.speakerSensitivity);
    }

    if(this.props.powerIntoSpeaker === "") {
      powerIntoSpeaker = 1;
    } else {
      powerIntoSpeaker = parseFloat(this.props.powerIntoSpeaker);
    }

    if(this.props.distanceFromSpeaker === "") {
      distanceFromSpeaker = 1;
    } else {
      distanceFromSpeaker = parseFloat(this.props.distanceFromSpeaker);
    }

    if(this.props.amountOfSpeakers === "") {
      amountOfSpeakers = 1;
    } else {
      amountOfSpeakers = parseFloat(this.props.amountOfSpeakers);
    }

    if(process.env.NODE_ENV === "development") {
      console.log(speakerSensitivity, powerIntoSpeaker, distanceFromSpeaker, amountOfSpeakers);
    }

    var errorMsg;

    if(isNaN(speakerSensitivity) || speakerSensitivity < 1) {
      errorMsg = "Specified sensitivity is invalid"
    } else if(isNaN(powerIntoSpeaker) || powerIntoSpeaker < 1) {
      errorMsg = "Specified power is invalid"
    } else if(isNaN(distanceFromSpeaker) || distanceFromSpeaker < 1) {
      errorMsg = "Specified distance is invalid"
    } else if(isNaN(amountOfSpeakers)) {
      errorMsg = "Specified speaker amount is invalid"
    }

    if(!errorMsg) {
      adjustedDecibel = (
        10 * Math.log10(parseFloat(powerIntoSpeaker)) +
        parseFloat(speakerSensitivity) +
        (3 * parseFloat(amountOfSpeakers-1))
      );
    
      adjustedDecibel = this.compensateForDistance(adjustedDecibel, distanceFromSpeaker);
    }

    var resultJsx;

    if(errorMsg) {
      resultJsx = (
        <React.Fragment>
          <h5>Result after accounting for all the filled in values:</h5>
          <div className="text-center">
            <span className="pt-0 pb-2 d-inline-block">{errorMsg}</span>
          </div>
        </React.Fragment>
      );
    } else {
      resultJsx = (
        <React.Fragment>
          <h5>Result after accounting for all the filled in values:</h5>
          <div className="text-center">
            <span id="decibel-number" className="pt-0 pb-2 d-inline-block">{adjustedDecibel > 0 ? adjustedDecibel.toFixed(1) : 0.0}</span><span> dB</span>
          </div>
        </React.Fragment>
      );
    }
    return(
      <div className="col-sm text-center pt-5">
        <span id="adjusted-decibel" className="result-section px-5 pt-2">
          {resultJsx}
        </span>
      </div>
    );
  }
}

export default DecibelAfterAmplification;