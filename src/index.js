import 'jquery';
import 'popper.js';
import 'bootstrap';
import './index.scss';
import React from 'react'
import { render } from 'react-dom'
import Root from './components/Root'

render(
    <Root />,
    document.getElementById('root')
)